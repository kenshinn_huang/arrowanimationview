package com.example.kenshinn.testarrowanimation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by kenshinn on 14/12/29.
 */
public class ArrowAnimationView extends View {

    private Bitmap mArrowBitmap = null;
    private Bitmap mBitmap = null;

    private Paint mPaint = new Paint();
    private int mFps = 10; // 1~60
    private int mRepeatCount = 2;
    private int mAnimationAngle = 90; // 0:right, 90: up, 180: left, 270: down
    private int mRotateForBitmap = 90; // rotate for arrow bitmap, counter-clock
    private int mArrowDistance = -70;
    private int mArrowCount = 3;
    private int mCurrentRepeat = 0;
    private float[] mArrowState = null;
    private boolean mEnable = false;
    private AnimationListener mListener = null;
    private Object syncObject = new Object();
    private boolean mPrepareNext = false;

    private Runnable mNextRunnable = new Runnable() {

        @Override
        public void run() {
            if(!mPrepareNext) {
                return;
            }

            for (int j = mArrowState.length - 1; j > 0; j--) {
                mArrowState[j] = mArrowState[j - 1];
            }

            //canvas.restore();
            if (mArrowState[mArrowState.length - 1] >= 1.0f) {
                if (++mCurrentRepeat < mRepeatCount) {
                    resetState();
                    //Log.d("kenshinn", "invalidate");
                    ArrowAnimationView.this.invalidate();
                } else {
                    mEnable = false;
                    ArrowAnimationView.this.invalidate();
                    if (mListener != null) {
                        mListener.onAnimationEnd(ArrowAnimationView.this);
                    }
                    resetState();
                    mCurrentRepeat = 0;
                }
            } else {
                //Log.d("kenshinn", "=============");
                ArrowAnimationView.this.invalidate();
            }

            mPrepareNext = false;
        }
    };

    public ArrowAnimationView(Context context) {
        super(context);
    }

    public ArrowAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArrowAnimationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void resetState() {
        for(int i = 0; i < mArrowState.length; i++) {
            mArrowState[i] = -1.1f;
        }
    }

    private void initState() {
        synchronized (syncObject) {
            if (mArrowState == null || mArrowState.length != mArrowCount) {
                mArrowState = new float[mArrowCount];
            }
            for (int i = 0; i < mArrowState.length; i++) {
                mArrowState[i] = -1.1f;
            }
        }
    }

    private void initBitmap() {
        if (mRotateForBitmap != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(-mRotateForBitmap);
            mBitmap = Bitmap.createBitmap(mArrowBitmap, 0, 0, mArrowBitmap.getWidth(), mArrowBitmap.getHeight(), matrix, true);
        } else {
            mBitmap = mArrowBitmap;
        }
    }

    public Bitmap getArrowBitmap() {
        return mArrowBitmap;
    }

    /**
     * set arrow bitmap
     * @param mArrowBitmap
     */
    public void setArrowBitmap(Bitmap mArrowBitmap) {
        this.mArrowBitmap = mArrowBitmap;
        initBitmap();
    }

    public int getFPS() {
        return mFps;
    }

    /**
     * set fps (1~60)
     * @param fps
     */
    public void setFPS(int fps) {
        if(fps <1 || fps > 60) {
            throw new IllegalArgumentException("fps must between 1~60");
        }
        this.mFps = fps;
    }

    public int getArrowCount() {
        return mArrowCount;
    }

    /**
     * set arrow count, must gather than 0
     * @param mArrowCount
     */
    public void setArrowCount(int mArrowCount) {
        if(mArrowCount <=0) {
            throw new IllegalArgumentException("arrow count must gather than 0");
        }
        this.mArrowCount = mArrowCount;
        synchronized (syncObject) {
            //this.mArrowState = new float[mArrowCount];
            initState();
        }
    }

    public int getRepeatCount() {
        return mRepeatCount;
    }

    /**
     * set repeat count
     * @param mRepeatCount
     */
    public void setRepeatCount(int mRepeatCount) {
        this.mRepeatCount = mRepeatCount;
    }

    public int getAnimationAngle() {
        return mAnimationAngle;
    }

    /**
     * set animation angle, 0:right, 90: up, 180: left, 270: down
     * @param mAnimationAngle
     */
    public void setAnimationAngle(int mAnimationAngle) {
        this.mAnimationAngle = mAnimationAngle;
    }

    public int getRotateForBitmap() {
        return mRotateForBitmap;
    }

    /**
     * set rotate angle for arrow bitmap, 0:right, 90: up, 180: left, 270: down
     * @param mRotateForBitmap
     */
    public void setRotateForBitmap(int mRotateForBitmap) {
        this.mRotateForBitmap = mRotateForBitmap;
        initBitmap();
    }

    public int getArrowDistance() {
        return mArrowDistance;
    }

    /**
     * set distance between every arrow
     * @param mArrowDistance
     */
    public void setArrowDistance(int mArrowDistance) {
        this.mArrowDistance = mArrowDistance;
    }

    public AnimationListener getAnimationListener() {
        return mListener;
    }

    /**
     * set animation listener
     * @param mListener
     */
    public void setAnimationListener(AnimationListener mListener) {
        this.mListener = mListener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(!mEnable || mArrowBitmap == null) {
            return;
        }

        synchronized (syncObject) {
            float change = 1.0f / mFps;
            mArrowState[0] += change;

            //canvas.save();
            for (int i = 0; i < mArrowState.length; i++) {
                Log.d("kenshinn", "i: " + mArrowState[i]);
                float state = mArrowState[i];
                if (state <= -1.1f) {
                    break;
                }
                float alpha = 1 - Math.abs(state);
                if (alpha < 0) {
                    alpha = 0;
                }

                if (alpha > 1) {
                    alpha = 1;
                }

                mPaint.setAlpha((int) (255 * alpha));
                Matrix matrix = new Matrix();

                float transX = 0;
                float transY = 0;

                switch (mAnimationAngle) {
                    case 0:
                    default:
                        transY = 0;
                        transX = i * (mBitmap.getWidth() + mArrowDistance);
                        break;
                    case 90:
                        transX = 0;
                        transY = -i * (mBitmap.getHeight() + mArrowDistance) + getMeasuredHeight() - mBitmap.getHeight();

                        break;
                    case 180:
                        transY = 0;
                        transX = -i * (mBitmap.getWidth() + mArrowDistance) + getMeasuredWidth() - mBitmap.getWidth();
                        break;
                    case 270:
                        transX = 0;
                        transY = i * (mBitmap.getHeight() + mArrowDistance);
                        break;
                }
                matrix.setTranslate(transX, transY);
                //matrix.setRotate(mRotateForBitmap, mArrowBitmap.getWidth()/2, mArrowBitmap.getHeight()/2);

                Log.d("kenshinn", "draw alpha: " + alpha + ",transx: " + transX + ",transy: " + transY);
                canvas.drawBitmap(mBitmap, matrix, mPaint);
            }
            Log.d("kenshinn", "==================");

            if(!mPrepareNext) {
                mPrepareNext = true;
                this.postDelayed(mNextRunnable, 1000/mFps);
            }

        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(mArrowBitmap != null) {
            int arrowWidth = 0;
            int arrowHeight = 0;
            if(mRotateForBitmap == 0 || mRotateForBitmap == 180) {
                arrowWidth = mArrowBitmap.getWidth();
                arrowHeight = mArrowBitmap.getHeight();
            } else {
                // rotate
                arrowWidth = mArrowBitmap.getHeight();
                arrowHeight = mArrowBitmap.getWidth();
            }

            int totalWidth = 0;
            int totalHeight = 0;
            if(mAnimationAngle == 0 || mAnimationAngle == 180) {
                totalWidth = mArrowCount*arrowWidth + (mArrowCount-1)*mArrowDistance;
                totalHeight = arrowHeight;
            } else {
                // rotate
                totalWidth = arrowWidth;
                totalHeight = mArrowCount*arrowHeight + (mArrowCount-1)*mArrowDistance;
            }

            setMeasuredDimension(totalWidth, totalHeight);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

    }

    public void start() {
        if(!mEnable) {
            if(mArrowState == null) {
                initState();
            }
            mEnable = true;
            this.postInvalidate();
            if(mListener != null) {
                mListener.onAnimationStart(this);
            }
        }
    }

    public void end() {
        if(mEnable) {
            mEnable = false;
            this.postInvalidate();
            resetState();
            mCurrentRepeat = 0;
        }
    }

    public interface AnimationListener {
        void onAnimationStart(ArrowAnimationView view);
        void onAnimationEnd(ArrowAnimationView view);
    }
}
