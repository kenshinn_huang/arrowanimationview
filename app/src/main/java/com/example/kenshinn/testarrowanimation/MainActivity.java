package com.example.kenshinn.testarrowanimation;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;


public class MainActivity extends Activity {

    private Button mStartButton;
    private CheckBox mLoopCheckbox;
    private EditText mRepeatText;
    private EditText mDistanceText;
    private EditText mImageAngleText;
    private EditText mAnimationAngleText;
    private EditText mFpsText;
    private EditText mArrowCountText;
    private ArrowAnimationView arrowAnimationView;

    private void initLayout() {
        mStartButton = (Button)findViewById(R.id.startButton);
        mLoopCheckbox = (CheckBox)findViewById(R.id.loopCheckbox);
        mRepeatText = (EditText)findViewById(R.id.repeatText);
        mDistanceText = (EditText)findViewById(R.id.distanceText);
        mImageAngleText = (EditText)findViewById(R.id.imageAngel);
        mAnimationAngleText = (EditText)findViewById(R.id.animationAngleText);
        mFpsText = (EditText)findViewById(R.id.fpsText);
        mArrowCountText = (EditText)findViewById(R.id.arrorCountText);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewGroup layout1 = (ViewGroup)findViewById(R.id.layout1);
        arrowAnimationView = new ArrowAnimationView(this);
        initLayout();

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mStartButton.getText().equals("start")) {

                    arrowAnimationView.setRepeatCount(Integer.parseInt(mRepeatText.getText().toString()));
                    arrowAnimationView.setArrowDistance(Integer.parseInt(mDistanceText.getText().toString()));
                    arrowAnimationView.setRotateForBitmap(Integer.parseInt(mImageAngleText.getText().toString()));
                    arrowAnimationView.setAnimationAngle(Integer.parseInt(mAnimationAngleText.getText().toString()));
                    arrowAnimationView.setFPS(Integer.parseInt(mFpsText.getText().toString()));
                    arrowAnimationView.setArrowCount(Integer.parseInt(mArrowCountText.getText().toString()));
                    arrowAnimationView.requestLayout();
                    arrowAnimationView.start();

                    mStartButton.setText("end");
                } else {
                    arrowAnimationView.end();
                    mStartButton.setText("start");
                }
            }
        });

        arrowAnimationView.setAnimationListener(new ArrowAnimationView.AnimationListener() {
            @Override
            public void onAnimationStart(ArrowAnimationView v) {
            }

            @Override
            public void onAnimationEnd(ArrowAnimationView v) {
                if (mLoopCheckbox.isChecked() && mStartButton.getText().equals("end")) {
                    //arrowAnimationView.postDelayed(new Runnable() {
                    //    @Override
                    //    public void run() {
                            arrowAnimationView.start();
                    //    }
                    //} , 500);
                } else {
                    mStartButton.setText("start");
                }
            }
        });
        layout1.addView(arrowAnimationView);

        arrowAnimationView.setArrowBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.arrow));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
